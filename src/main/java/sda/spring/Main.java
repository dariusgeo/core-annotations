package sda.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import sda.spring.config.AppConfig;
import sda.spring.service.Launcher;

public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Launcher launcher = context.getBean(Launcher.class);
		
		launcher.execute();
		//context.publishEvent(event);
	}

}
