package sda.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import sda.spring.service.Launcher;

@Configuration
@ComponentScan(basePackages = {"sda.spring"})
public class AppConfig {
	
	@Bean
	public Launcher getLauncher(){
		return new Launcher();
	}
}
