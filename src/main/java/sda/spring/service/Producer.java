package sda.spring.service;

import org.springframework.stereotype.Component;

@Component
public class Producer {

	public void sendMessage(){
		System.out.println("Sending message...");
		for (int i = 0 ; i < 5 ; i++){
			try {
				Launcher.QUEUE.put(i);
			} catch (InterruptedException e) {
				System.out.println("Adding elements into queue ... " + e.getMessage());
			}
		}
	}
}
