package sda.spring.service;

import java.util.concurrent.ArrayBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;

public class Launcher {
	public static final ArrayBlockingQueue<Integer> QUEUE = new ArrayBlockingQueue<>(5);
	
	@Autowired
	private Producer producer;
	
	@Autowired
	private Consumer consumer;
	
	public void execute(){
		System.out.println("Starting...");
		producer.sendMessage();
		consumer.consumeMessage();
	}
}
